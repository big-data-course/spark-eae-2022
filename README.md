## Descargar el proyecto

Pulsad sobre el botón "..." y elegir "Download repository" para descargar el fichero zip

Descomprimid el fichero zip dentro del directorio `bigdata` que tenéis en el Escritorio.

## Descargar la imagen de docker

Una vez descomprimido el zip, deberíamos tener la siguiente estructura:

```bash
spark-eae-2022
├── docker-compose.yml
├── README.md
└── work
    ├── confSpark
    └── spark
```

Nos situamos dentro del directorio `spark-eae-2002` y nos descargamos la imagen de docker:

```
docker-compose pull
```

Este comando descargará la imagen `jupyter/pyspark-notebook` que ocupa 3.8GB

