from localFile import toUri
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pprint_extended import nprint

#Definición del spark context y el streaming context
sc = SparkContext('local[*]','wc_socket')
ssc = StreamingContext (sc, 10) #Se activa cada 2 segundos

#Definición del socket stream en la ip "localhost" y puerto 9999
lines = ssc.socketTextStream ("localhost", 9999)

#Leemos las líneas y las separamos en palabras distintas
words = lines.flatMap (lambda x: x.split (' '))

#Transformamos a par palabra, 1
pairs = words.map(lambda word: (word, 1))
#Aplicamos reduceByKey para ver las veces que aparece cada palabra (key)
wordCounts = pairs.reduceByKey(lambda x, y: x + y)

wordCounts.pprint()
wordCounts.saveAsTextFiles (toUri('./tmp/wc_socket_toFile_out'))

ssc.start()             # Start the computation
try:
    ssc.awaitTermination()  # Wait for the computation to terminate
except:
    pass

ssc.stop(stopSparkContext=True,stopGraceFully=True)

