from datetime import datetime
def nprint (dstream, text, num=10, show_empty=True):
    #Código general a todos los RDD's
    
    def takeAndPrint (time, rdd):
        taken = rdd.take (num + 1)
        if taken or show_empty:
            print ("----------------------------------------------")
            print (f'({time}) {text}')
            print ("----------------------------------------------")
            for record in taken[:num]:
                print (record)
            if len(taken) > num:
                print ('...')
            print ('')
        
    dstream.foreachRDD (takeAndPrint)


