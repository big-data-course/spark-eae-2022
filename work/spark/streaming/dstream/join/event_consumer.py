from localFile import toUri
from pprint_extended import nprint
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from datetime import datetime

import argparse

def parse_entry(msg):
    """
    Event TCP sends sends data in the format
    timestamp:event\n
    """
    values = msg.split(';')
    return {
        'dt': datetime.strptime(
            values[0], '%Y-%m-%d %H:%M:%S.%f'),
        'event': values[1]
    }


def aggregate_by_event_type(record):
    """
    Step 1. Maps every entry to a dictionary.
    Step 2. Transform the dataset in a set of
        tuples (event, 1)
    Step 3: Applies a reduction by event type
        to count the number of events by type
        in a given interval of time.
    """
    return record.map(parse_entry)\
        .map(lambda record: (record['event'], 1))\
        .reduceByKey(lambda a, b: a+b)

def update_global_event_counts(key_value_pairs):
    def updateFunction(newValues, runningCount):
        if runningCount is None:
            runningCount = 0
        return sum(newValues, runningCount)  # add the new values with the previous running count to get the new count

    return key_value_pairs.updateStateByKey(updateFunction)


def consume_records(
        interval=5, host='localhost', port1=9876, port2=9875):
    """
    Create a local StreamingContext with two working
    thread and batch interval of 1 second
    """
    spark_context = SparkContext(appName='LogSocketConsumer')
    stream_context = StreamingContext(spark_context, interval)
    stream_context.checkpoint(toUri('/tmp'))

    stream1 = stream_context.socketTextStream(host, port1)
    stream2 = stream_context.socketTextStream(host, port2)

    # counts number of events
    event_counts1 = aggregate_by_event_type(stream1)
    nprint(event_counts1, "Agrupación puerto "+str(port1))

    event_counts2 = aggregate_by_event_type(stream2)
    nprint(event_counts2, "Agrupación puerto "+str(port2))

    
    event_join = event_counts1.join(event_counts2)
    nprint (event_join, "***   JOIN  *****")
    
    stream_context.start()
    try:
        stream_context.awaitTermination()
    except:
        pass
    
    stream_context.stop(stopSparkContext=True,stopGraceFully=True)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--interval', required=False, default=10.0,
        help='Interval in seconds (default=10)', type=float)

    parser.add_argument(
        '--port1', required=False, default=9876,
        help='Port (Default=9876)', type=int)

    parser.add_argument(
        '--port2', required=False, default=9875,
        help='Port (Default=9875)', type=int)

    parser.add_argument(
        '--host', required=False, default='localhost', help='Host')

    args, extra_params = parser.parse_known_args()
    consume_records(
        interval=args.interval, port1=args.port1, port2=args.port2, host=args.host)


if __name__ == '__main__':
    main()
