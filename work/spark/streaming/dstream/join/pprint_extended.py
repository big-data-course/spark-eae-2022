from datetime import datetime
def nprint (dstream, text, num=10):
    
    def takeAndPrint (time, rdd):
        taken = rdd.take (num + 1)
        print ("----------------------------------------------")
        print (f'({time}) {text}')
        print ("----------------------------------------------")

        for record in taken[:num]:
            print (record)
        if len(taken) > num:
            print ('...')
        print ('')
        
    dstream.foreachRDD (takeAndPrint)
    