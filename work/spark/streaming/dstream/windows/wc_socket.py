from localFile import toUri
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pprint_extended import nprint

#Definición del spark context y el streaming context
sc = SparkContext('local[*]','wc_socket')
ssc = StreamingContext (sc, 3)

#Para las operaciones de ventana es necesario definir
#El directorio de checkpoint
ssc.checkpoint(toUri('/tmp'))


#Definición del socket stream en la ip "localhost" y puerto 9999
lines = ssc.socketTextStream ("localhost", 9999)

#Leemos las líneas y las separamos en palabras distintas
words = lines.flatMap (lambda x: x.split (' '))

#Transformamos a par palabra, 1
pairs = words.map(lambda word: (word, 1))

#Aplicamos reduceByKeyAndWindow para ver las veces que aparece cada palabra (key)
#La repetición la analiza en la ventana de los últimos 6 segundos y
#la ventana la va desplazando cada 3 segundos
def addKey (x, y):
    return x+y

def subKey (x,y):
    return x-y

wordCounts = pairs.reduceByKeyAndWindow(addKey, subKey, 12, 6)

#Mostramos el contenido del DStream
nprint(wordCounts, 'Prueba')
#wordCounts.pprint()


ssc.start()             # Start the computation
try:
    ssc.awaitTermination()  # Wait for the computation to terminate
except:
    pass

ssc.stop(stopSparkContext=True,stopGraceFully=True)

