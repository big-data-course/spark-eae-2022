from datetime import datetime
def nprint (dstream, text, num=10):
    
    def takeAndPrint (time, rdd):
        taken = rdd.take (num + 1)
        print ("----------------------------------------------")
        print (f'({time}) {text}')
        print ("----------------------------------------------")

        for record in taken[:num]:
            print (record)
        if len(taken) > num:
            print ('...')
        print ('')
        
    dstream.foreachRDD (takeAndPrint)

def to_csv (dstream,file_name):
    idx = 0
    def writePartition (iter):
        # with open(file_name, 'w', newline='') as myfile:
        #     wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        #     for record in iter:
        #         wr.writerow(record)
        print (f'Graba particion {idx}')
        print (f'========================')
        for record in iter:
            print (record)
        idx += 1

    dstream.foreachRDD (lambda rdd: rdd.foreachPartition(writePartition))
