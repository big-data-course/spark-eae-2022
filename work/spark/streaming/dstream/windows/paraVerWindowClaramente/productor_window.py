from datetime import datetime

import argparse
import random
import time
import socket

def create_data():
    random.seed(datetime.utcnow().microsecond)
    dt = datetime.utcnow()\
        .strftime('%H:%M:%S')
    punto = random.randint(1,11)
    return '{} {}\n'.format(dt, punto).encode('utf-8')

def initialize(port=9876, interval=1):
    """
    Initialize a TCP server that returns a non deterministic
    flow of simulated events to its clients
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', port)
    sock.bind(server_address)
    sock.listen(5)
    print("Listening at {}".format(server_address))
    try:
        connection, client_address = sock.accept()
        print('connection from', client_address)
        while True:
            line = create_data()
            connection.sendall(line)
            print(line)
            time.sleep(interval)
    except Exception as e:
        print(e)

    finally:
        sock.close()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--port', required=False, default=9999,
        help='Port (default=9999)', type=int)

    parser.add_argument(
        '--interval', required=False, default=1,
        help='Interval in seconds (default=1)', type=float)

    args, extra_params = parser.parse_known_args()
    initialize(port=args.port, interval=args.interval)


if __name__ == '__main__':
    main()
