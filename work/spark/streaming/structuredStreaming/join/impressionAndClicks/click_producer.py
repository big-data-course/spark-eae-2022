from datetime import datetime, timedelta

import argparse
import random
import time
import socket
import json



def get_data():
    random.seed(datetime.utcnow().microsecond)
    data = {}
    randomSeg = timedelta(seconds=random.uniform(40,0))
    newDatetime = datetime.utcnow() - randomSeg
    
    data['adId'] = int(newDatetime.strftime("%M:%S").replace(':','')[:3]+'0')
    data['time'] = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    return data

def get_line():
    data = get_data()
    return json.dumps(data)+"\n"


def randomize_interval(interval):
    """
    Returns a random value sligthly different
    from the orinal interval parameter
    """
    random.seed(datetime.utcnow().microsecond)
    delta = interval + random.uniform(-0.1, 0.9)
    # delay can not be 0 or negative
    if delta <= 0:
        delta = interval
    return delta


def initialize(port=9876, interval=0.5):
    """
    Establish a connection with a stream socket consumer
    and push simulated log entries.
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', port)
    print("Listening at {}".format(server_address))
    sock.bind(server_address)
    sock.listen(5)
    try:
        connection, client_address = sock.accept()
        print('connection from', client_address)
        while True:
            line = get_line().encode('utf-8')
            connection.sendall(line)
            print(line)
            time.sleep(randomize_interval(interval))
    except Exception as e:
        print (e)
    finally:
        sock.close()

def main():
    """
    Push data to a TCP socket
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--port', required=False, default=9876,
        help='Port', type=int)

    parser.add_argument(
        '--interval', required=False, default=0.5,
        help='Interval in seconds', type=float)

    parser.add_argument(
        '--onlyPrint', required=False, action='store_true',
        help='Not send message to port')

    args, _ = parser.parse_known_args()

    if args.onlyPrint:
        while True:
            line = get_line()
            print('[onlyPrint]'+line)
            time.sleep(randomize_interval(args.interval))
    else:
        initialize(port=args.port, 
                interval=args.interval)

    

if __name__ == '__main__':
    main()
