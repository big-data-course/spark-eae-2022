from pyspark.sql import SparkSession
from pyspark.sql.functions import expr, lit, from_json, count
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, TimestampType
import argparse




def start_consummer(spark, portClicks, portImpressions):

    adSchema = StructType(
        [
            StructField ('adId', IntegerType(), False),
            StructField ('time', TimestampType(), False)
        ]
    )
    clicks = spark \
        .readStream \
        .load( format="socket",
            host="localhost",
            port=portClicks
            )
    impressions = spark\
        .readStream\
        .load( format="socket",
            host="localhost",
            port=portImpressions
            )

    clicksWM = clicks.select(from_json('value', schema=adSchema).alias('data'))\
        .selectExpr ('data.adId as clickAdId', 'data.time as clickTime')\
        .withWatermark ("clickTime", "10 seconds")

    impressionsWM = impressions.select(from_json('value', schema=adSchema).alias('data'))\
        .selectExpr ('data.adId as impressionAdId', 'data.time as impressionTime')\
        .withWatermark ("impressionTime", "20 seconds")
    """ 

     Inner
     =====
        - Se puede ejecutar sin watermark ni condición de tiempo


     Leftouter
     ==============================================
        - Se puede ejecutar sin watermark ni condición de tiempo
        - watermark en el DF derecho y condición de restricción por columna watermark
        - watermark en los dos y condición de restricción por columnas watermark

        and
                clickTime >= impressionTime and
                clickTime <= impressionTime + interval 10 seconds
    """   
    query = impressionsWM.join(
        clicksWM, #expr('clickAdId = impressionAdId'), 
        expr("""
            clickAdId = impressionAdId         and
                clickTime >= impressionTime and
                clickTime <= impressionTime + interval 10 seconds

        """),
        'leftOuter'
    ).writeStream\
    .outputMode("append")\
    .trigger(processingTime='0 seconds')\
    .format("console")\
    .start()

    try:
        query.awaitTermination()
    except Exception as e:
        print (e)

def main(portClicks, portImpressions):
    spark = SparkSession \
    .builder \
    .appName("clickImpression_consumer") \
    .getOrCreate()

    start_consummer(spark, portClicks, portImpressions)
    #spark.stop()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--portClicks', required=False, default=9876,
        help='Port for click input', type=int)
    parser.add_argument(
        '--portImpressions', required=False, default=9875,
        help='Port for click input', type=int)
    
    args, _ = parser.parse_known_args()

    main(args.portClicks, args.portImpressions)

