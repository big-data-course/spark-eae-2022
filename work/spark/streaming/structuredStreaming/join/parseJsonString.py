from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import StructType, StructField, StringType, TimestampType, DateType

spark = SparkSession \
    .builder \
    .appName("clickImpression_consumer") \
    .getOrCreate()

data = ["{'ad':'1', 'time':'2020-03-26 23:28:12', 'hora':'12:20'}"]
df = spark.createDataFrame (data,StringType())

jsonSchema = StructType ([
    StructField ('ad', StringType()),
    StructField ('time', TimestampType()),
    StructField ('hora', StringType())
])

ndf=df.withColumn("data", from_json(df.value,jsonSchema)).selectExpr ('data.ad as ad', 
    'data.time as time',
    'from_unixtime(unix_timestamp(data.hora,"HH:mm")) as hora')
ndf.printSchema()

ndf.show()

spark.stop()