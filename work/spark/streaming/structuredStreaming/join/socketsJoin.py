from pyspark.sql import SparkSession
from pyspark.sql.functions import explode, split, lit, col

spark = SparkSession \
    .builder \
    .appName("socketUnionGroup") \
    .getOrCreate()

lines1 = spark \
    .readStream \
    .load( format="socket",
           host="localhost",
           port=9999
         )

words1 = lines1.select(
    explode(split(lines1.value, " ")).alias('word')
).withColumn('puerto',lit(9999))


lines2 = spark\
    .readStream\
    .load( format="socket",
           host="localhost",
           port=9998
         )

words2 = lines2.select(
    explode(split(lines2.value, " ")).alias('word')
).withColumn('puerto',lit(9998))


#hay que incluir watermark
query = words1.join(words2, "word","inner")\
    .writeStream\
    .outputMode("append")\
    .trigger(processingTime = '20 seconds')\
    .format("console")\
    .start()

try:
    query.awaitTermination()
except:
    pass

