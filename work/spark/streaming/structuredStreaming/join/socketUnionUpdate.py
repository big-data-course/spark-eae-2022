from pyspark.sql import SparkSession
from pyspark.sql.functions import explode, split, lit, col

spark = SparkSession \
    .builder \
    .appName("socketUnionUpdate") \
    .getOrCreate()

lines1 = spark \
    .readStream \
    .load( format="socket",
           host="localhost",
           port=9999
         )

words1 = lines1.select(
    explode(split(lines1.value, " ")).alias('word')
).withColumn('puerto',lit('p1'))


lines2 = spark\
    .readStream\
    .load( format="socket",
           host="localhost",
           port=9998
         )

words2 = lines2.select(
    explode(split(lines2.value, " ")).alias('word')
).withColumn('puerto',lit('p2'))


#Al tener update no agrega los contenidos anteriores, solo los de la ventana
#Deberían producirse los datos de forma simultánea para que se unieran los
#de los dos puertos.
query = words1.union(words2).groupBy(col('puerto'), col('word')).count()\
    .writeStream\
    .outputMode("update")\
    .format("console")\
    .start()

try:
    query.awaitTermination()
except:
    pass

