from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, TimestampType
import argparse




def start_consummer(spark, port, period):

    #Estructura de los datos que llegan por el puerto
    eventSchema = StructType(
        [
            StructField ('time', StringType(), False),
            StructField ('eventTime', StringType(), False),
            StructField ('word', StringType(), False)
        ]
    )

    #Definición de stream
    events = spark \
        .readStream \
        .load( format="socket",
            host="localhost",
            port=port
            )

    #Definición del tamaño de la ventana
    #window_size = str(period*20)+" seconds"
    #Definición del desplazamiento de la ventana
    #sliding = str(period*10)+" seconds"
    #print (f'Ventana de {window_size}, simula 20 minutos')
    #print (f'Desplazamiento cada {sliding}, simula 10 minutos')

    # Realizamos un parseo de los datos que vienen en un string con formato
    # json a columnas individuales dentro de la columna data.
    # 
    # Incluimos un watermark con el tamaño de la ventana de tiempo
    # Definimos el groupBy por la ventana y por 'word'
    eventsWM = events\
        .select(F.from_json('value', schema=eventSchema).alias('data'))\
        .select(
            F.from_utc_timestamp(
                F.from_unixtime(F.unix_timestamp(F.col('data.time'),"HH:mm:ss")), 
                "GMT"
            ).alias('send_time'), 
            F.from_utc_timestamp(
                F.from_unixtime(F.unix_timestamp(F.col('data.eventTime'),"HH:mm")), 
                "GMT"
            ).alias('event_time'), 
            F.col('data.word').alias('word')
        )\
        .withWatermark ("event_time", "20 minutes")\
        .groupBy (
            F.window("event_time", "20 minutes", "10 minutes"),
            'word'
        ).count()
    
    eventsOut = eventsWM.select (
        F.concat(F.hour('window.start'),F.lit(':'),F.minute('window.start'),
                 F.lit('-'),
                 F.hour('window.end'),F.lit(':'),F.minute('window.end')
                ).alias('window'),
        'word',
        'count'
    ).orderBy('window')

    
    query = eventsOut\
    .writeStream\
    .outputMode("complete")\
    .trigger(processingTime='0 seconds')\
    .format("console")\
    .start()

    try:
        query.awaitTermination()
    except Exception as e:
        print (e)

def main(port, period):
    spark = SparkSession \
    .builder \
    .appName("clickImpression_consumer") \
    .getOrCreate()

    start_consummer(spark, port, period)
    #spark.stop()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--port', required=False, default=9876,
        help='Port for input data', type=int)
    parser.add_argument(
        '--period', required=False, default=1,
        help='Duration of period time in seconds(default 1)', type=int)
    
    args, _ = parser.parse_known_args()

    main(args.port, args.period)

