from datetime import datetime

import argparse
import random
import time
import socket
DATA =(
## (num_periodos_espera, (hora_emision,palabra))
    (7,  ('12:07', 'dog')),
    (8,  ('12:08', 'owl')),
    (12, ('12:14', 'dog')),
    (13, ('12:09', 'cat')),
    (16, ('12:15', 'cat')),
    (17, ('12:08', 'dog')),
    (18, ('12:13', 'owl')),
    (19, ('12:21', 'owl')),
    (23, ('12:04', 'donkey')),
    (0, ('stop')),
)
def get_line(data):
    """
    Returns a string with the current timestamp
    and a random event chosen from a list
    of events at random
    """
    time = datetime.utcnow()\
        .strftime('%H:%M:%S')
    eventTime = data[0]
    event = data[1]
    return f'{{"time":"{time}", "eventTime":"{eventTime}", "word":"{event}"}}\n'.encode('utf-8')

def initialize(port=9876, period=1):
    """
    Initialize a TCP server that returns a non deterministic
    flow of simulated events to its clients
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', port)
    sock.bind(server_address)
    sock.listen(5)
    print("Listening at {}".format(server_address))
    try:
        connection, client_address = sock.accept()
        print('connection from', client_address)
        print (f'Inicio de proceso {datetime.utcnow().strftime("%H:%M:%S.%f")}')
        antDelay = 0
        for event in DATA:
            if event[0]>0:
                delay = event[0]*60
                time.sleep (period*(delay-antDelay))
                line = get_line(event[1])
                print (f'Minuto {event[0]}--> {line}')
                connection.sendall (line)
                antDelay = delay
    except Exception as e:
        print(e)

    finally:
        sock.close()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--port', required=False, default=9876,
        help='Port', type=int)

    parser.add_argument(
        '--period', required=False, default=1,
        help='Duration of period time in seconds(default 1)', type=int)

    parser.add_argument(
        '--onlyPrint', required=False, action='store_true',
        help='Not send message to port')

    args, _ = parser.parse_known_args()
    if args.onlyPrint:
        print (f'Inicio de proceso {datetime.utcnow().strftime("%H:%M:%S.%f")}')
        antDelay=0        
        for event in DATA:
            if event[0]>0:
                time.sleep (args.period*(event[0]-antDelay))
                line = get_line(event[1])
                print (f'Minuto {event[0]}--> {line}')
                antDelay = event[0]
    else:
        initialize(port=args.port, 
                period=args.period)

    

if __name__ == '__main__':
    main()
