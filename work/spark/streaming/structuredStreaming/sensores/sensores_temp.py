#!/usr/bin/env python
# coding: utf-8

# ## Emisor de temperaturas
# 
# Debemos generar temperaturas entre 12 y 30 grados para 10 sensores distintos.
# 
# Los sensores se identificarán por el patrón S*, siendo el asterisco de 1 a 10
# 
# ```json
# {'id': 'S1', 'date_time' = '2020-04-20 16:30:00', 'temp'=23}
# ```
# 
# json
# datetime
# random
# ...
# 

# In[2]:


import random
random.uniform(12,30)


# In[3]:


import random
import json
from datetime import datetime


# In[4]:


def read_sensor():
    """
    Simula lecturas de sensores
    {'id': 'S1', 'date_time' = '2020-04-20 16:30:00', 'temp'=23}
    """
    
    dt = datetime.utcnow()        .strftime("%Y-%m-%d %H:%M:%S")
    id = f'S{random.randint(1,11)}'
    temp = round(random.uniform(12,30),1)
    lectura = {'id': id,
               'dt': dt,
               'temp': temp}
    out = json.dumps(lectura)+'\n'
    return str(temp).encode('utf-8') 
    
    
    


# In[5]:


read_sensor()


# In[7]:


import time
import socket
port = 9998
debug = False
server_address = "debug"
client_address = "debug"


if not debug:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', port)
    sock.bind(server_address)
    sock.listen(5)
print("Listening at {}".format(server_address))
try:
    if not debug:
        connection, client_address = sock.accept()
    print('connection from', client_address)
    while True:
        interval = random.uniform(1,1.5)
        lectura = read_sensor()
        if not debug:
            connection.sendall(lectura)
        print(lectura)
        time.sleep(interval)
except Exception as e:
    print(e)

finally:
    sock.close()


# In[ ]:





# In[ ]:




