from pyspark.sql import SparkSession
from pyspark.sql.functions import explode
from pyspark.sql.functions import split

spark = SparkSession \
    .builder \
    .appName("StructuredNetworkWordCount") \
    .getOrCreate()

lines = spark \
    .readStream \
    .load( format="socket",
           host="localhost",
           port=9999
         )

words = lines.select(
    explode(split(lines.value, " ")).alias('word')
)

wordCounts = words.groupBy ('word').count()

query = wordCounts \
    .writeStream \
    .outputMode("complete") \
    .format("console") \
    .start()

try:
    query.awaitTermination()
except:
    pass

