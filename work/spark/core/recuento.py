from pyspark import SparkContext
import re
sc = SparkContext('local[*]','RecuentoPalabras')
entrada = sc.textFile('hdfs://192.168.137.128:9000/books/Quijote.txt')
palabras2 = entrada.flatMap(lambda x: re.compile('\\W+').split(x))
palMinusculas = palabras2.map(lambda x: x.lower())
recuentoPal3 = palMinusculas.map(lambda x: (x,1)).reduceByKey(lambda x, y: x + y)
recOrdenado = recuentoPal3.sortBy(lambda a: a[1],ascending=False)
recOrdenado.saveAsTextFile('hdfs://192.168.137.128:9000/tmp/resultados2')